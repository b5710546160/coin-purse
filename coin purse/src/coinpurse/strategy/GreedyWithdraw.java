package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.main.Valuable;
import coinpurse.main.ValueComparator;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {
	/** Collection of coins in the purse. */
	private final ValueComparator comparator = new ValueComparator();

	/**
	 * Create a GreedyWithdraw with a specified capacity.
	 * 
	         
	 */
	public GreedyWithdraw(  ) {

    
    }

	/**
	 * Count and return the number of coins in the purse. This is the number of
	 * coins, not their value.
	 * 
	 * @return the number of coins in the purse
	 */
	
	/**
	 * Get the total value of all items in the purse.
	 * 
	 * @return the total value of items in the purse.
	 */
	
	


	/**
	 * 
	 * @param amount
	 *            value is we want to withdraw
	 * @param valuable
	 *            List is collect Valuable
	 * @return List
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuable) {
		if (amount < 0)
			return null;

		List<Valuable> with = new ArrayList<Valuable>();
		Collections.sort(valuable, comparator);
		//Collections.reverse(valuable);
		List<Valuable> coins2 = new ArrayList<Valuable>();
		ArrayList<Integer> number = new ArrayList<Integer>();
		coins2.addAll(valuable);
		double num = amount;
		// do{

		for (int i = 0; i < valuable.size(); i++) {
			if (valuable.get(i).getValue() <= num) {
				with.add(valuable.get(i));
				// coins2.remove(i);
				number.add(i);
				num -= valuable.get(i).getValue();

			}
		}

		if (num > 0) {

		} else {
			for (int i = number.size() - 1; i >= 0; i--) {
				valuable.remove(with.get(i));
			}
		}
		// }while(num!=0||first==0);

		/*
		 * One solution is to start from the most valuable coin in the purse and
		 * take any coin that maybe used for withdraw. Since you don't know if
		 * withdraw is going to succeed, don't actually withdraw the coins from
		 * the purse yet. Instead, create a temporary list. Each time you see a
		 * coin that you want to withdraw, add it to the temporary list and
		 * deduct the value from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter, use a local
		 * total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), then you are
		 * done. Now you can withdraw the coins from the purse. NOTE: Don't use
		 * list.removeAll(templist) for this becuase removeAll removes *all*
		 * coins from list that are equal (using Coin.equals) to something in
		 * templist. Instead, use a loop over templist and remove coins
		 * one-by-one.
		 */

		// Did we get the full amount?
		if (num > 0) { // failed. Since you haven't actually remove
						// any coins from Purse yet, there is nothing
						// to put back.
			List<Valuable> fall= new ArrayList<Valuable>();
			return fall;
		} else {
			List<Valuable> a = with;
			
			return a;
		}

	}

	/**
	 * @return a toString returns a string description of the purse contents. It
	 *         can return whatever is a useful description.
	 */
	
}
