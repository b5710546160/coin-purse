package coinpurse.strategy;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class ListUtil {
	/**
	 * 
	 * @param list
	 *            list we want to print.
	 */
	private static void printList(List<?> list) {
		int num = 0;
		String a = stringTo(list, num);
		System.out.print(a);
	}

	private static String stringTo(List<?> list, int num) {
		if (num == list.size())
			return "";
		else {
			String a;
			if (num < list.size() - 1) {
				a = list.get(num) + "," + stringTo(list, num + 1);
			} else {
				a = list.get(num) + stringTo(list, num + 1);
			}

			return a;
		}
	}

	private static String max(List<String> list) {
		String a = "";
		for (int i = 0; i < list.size(); i++) {
			String com=list.get(i);
			int n= (com.compareTo(a));
			if (n>1||i==0) {
				a = list.get(i);
			}
		}
		return a;
	}

	/** Test the max method. 
	 * @param args ...
	 * */
	public static void main(String[] args) {
		List<String> list;
		// if any command line args, then use them as the list!
		if (args.length > 0)
			list = Arrays.asList(args);
		else
			list = Arrays.asList("bird", "zebra", "cat", "pig");
		System.out.print("List contains: ");
		printList(list);
		String max = max(list);
		System.out.println("Lexically greatest element is " + max);
	}
}
