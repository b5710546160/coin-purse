package coinpurse.strategy;

import java.util.List;

import coinpurse.main.Valuable;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public interface WithdrawStrategy{
	/**
	 * 
	 * @param amount value is we want to withdraw
	 * @param valuable List is collect Valuable
	 * @return List
	 */
	public  List<Valuable> withdraw(double amount,List<Valuable> valuable); 
}
