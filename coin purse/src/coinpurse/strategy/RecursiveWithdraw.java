package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.main.Valuable;
import coinpurse.main.ValueComparator;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {
	private final ValueComparator comparator = new ValueComparator();

	/**
	 * Create a GreedyWithdraw with a specified capacity.
	 * 
	 *
	 */
	public RecursiveWithdraw() {

	}

	/**
	 * get withdrawvalue and list of deposit from Purse.
	 * 
	 * @param amount
	 *            value is we want to withdraw
	 * @param valuable
	 *            List is collect Valuable
	 * @return List remain
	 */
	public List<Valuable> withdraw(double amount, List<Valuable> valuable) {
		List<Valuable> result = new ArrayList<Valuable>();
		// Collections.sort(valuable, comparator);
		// Collections.reverse(valuable);
		/*
		 * int num =valuable.size(); double re=0; do{ num--; result =
		 * withdrawFrom(amount,valuable,num); if(result!=null){ re
		 * =Sum(result);} else{ re=0;} }while(re!=amount||num>=0);
		 */
		
		result = withdrawFrom(amount, valuable, valuable.size() - 1);
		if (result == null) {
			return null;
		} else {
			for (int i = result.size() - 1; i >= 0; i--) {
				valuable.remove(result.get(i));
			}
			return result;

		}
	}

	/**
	 * 
	 * @param amount
	 *            withdraw is we want
	 * @param list
	 *            list of deposit
	 * @param lastIndex
	 *            Index of now value
	 * @return withdraw list is we want to withdraw
	 */
	public List<Valuable> withdrawFrom1(double amount, List<Valuable> list,int lastIndex) {
		
		if (amount == 0) {
			List<Valuable> returningList = new ArrayList<Valuable>();
			return returningList;
		}
		if (lastIndex < 0&& amount != 0) {
			return null;
		}
		double value = list.get(lastIndex).getValue();
		if (value > amount) {
			return withdrawFrom1(amount, list, lastIndex - 1);

		}
		amount -= value;
		Valuable picked = list.get(lastIndex);
		List<Valuable> nextlist = withdrawFrom1(amount, list, lastIndex - 1);
	
		if (nextlist != null) {
			nextlist.add(picked);
		}
		else{
			nextlist= withdrawFrom1(amount + value, list, lastIndex - 1);
		}
		return nextlist;
	}
	/**
	 * 
	 * @param amount
	 *            withdraw is we want
	 * @param list
	 *            list of deposit
	 * @param lastIndex
	 *            Index of now value
	 * @return withdraw list is we want to withdraw
	 */
	public List<Valuable> withdrawFrom(double amount, List<Valuable> list,int lastIndex) {
		/*
		 * Collections.sort(list, comparator); Collections.reverse(list); for
		 * (int i = lastIndex; i >= 0; i--) { List<Valuable> result =
		 * withdrawFrom1(amount,list,i); if (result != null) { return result; }
		 * }
		 */
		if (amount >= 0) {
			List<Valuable> result = withdrawFrom1(amount, list,list.size() - 1);
			if (result != null) {
				return result;
			}
		}
		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result = withdrawFromlast(amount, list, i);
			if (result != null) {
				return result;
			}
		}
		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result = withdrawFromlast1(amount, list, i);
			if (result != null) {
				return result;
			}
		}
		Collections.sort(list, comparator);

		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result1 = withdrawFromlast(amount, list, i);
			if (result1 != null) {
				return result1;
			}
		}
		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result = withdrawFromlast1(amount, list, i);
			if (result != null) {
				return result;
			}
		}
		Collections.reverse(list);
		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result1 = withdrawFromlast(amount, list, i);
			if (result1 != null) {
				return result1;
			}
		}
		for (int i = lastIndex; i >= 0; i--) {
			List<Valuable> result = withdrawFromlast1(amount, list, i);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param amount
	 *            withdraw is we want
	 * @param list
	 *            list of deposit
	 * @param lastIndex
	 *            Index of now value
	 * @return withdraw list is we want to withdraw
	 */
	public List<Valuable> withdrawFromlast(double amount, List<Valuable> list,int lastIndex) {
		List<Valuable> last = new ArrayList<Valuable>();
		double value = amount - list.get(lastIndex).getValue();
		last.add(list.get(lastIndex));
		if (value == 0) {
			return last;
		}
		for (int i = lastIndex - 1; i >= 0; i--) {
			if (list.get(i).getValue() > value) {
				value += list.get(i + 1).getValue();
				last.remove(list.get(i + 1));
			}
			if (list.get(i).getValue() <= value) {
				last.add(list.get(i));
				value -= list.get(i).getValue();
			}
			/*
			 * else{ return null; }
			 */
			if (value == 0) {
				return last;
			}
		}
		/*
		 * if (sum(last,last.size()) == amount) { return last; }
		 */
		return null;
	}

	/**
	 * 
	 * @param amount
	 *            withdraw is we want
	 * @param list
	 *            list of deposit
	 * @param lastIndex
	 *            Index of now value
	 * @return withdraw list is we want to withdraw
	 */
	public List<Valuable> withdrawFromlast1(double amount, List<Valuable> list,int lastIndex) {
		List<Valuable> last = new ArrayList<Valuable>();
		double value = amount - list.get(lastIndex).getValue();
		last.add(list.get(lastIndex));
		if (value == 0) {
			return last;
		}
		for (int i = lastIndex - 1; i >= 0; i--) {
			/*
			 * if(list.get(i).getValue() > value) { value
			 * +=list.get(i+1).getValue(); last.remove(list.get(i+1)); }
			 */
			if (list.get(i).getValue() <= value) {
				last.add(list.get(i));
				value -= list.get(i).getValue();
			}
			/*
			 * else{ return null; }
			 */
			if (value == 0) {
				return last;
			}
		}
		/*
		 * if (sum(last,last.size()) == amount) { return last; }
		 */
		return null;
	}

	/**
	 * 
	 * @param result
	 *            list is we want to check sum
	 * @return sum
	 * @param index
	 *            number of now value
	 */
	public double sum(List<Valuable> result, int index) {
		if (index < 0) {
			return 0;
		}
		double sum = result.get(index).getValue() + sum(result, index - 1);
		return sum;
		/*
		 * double sum = 0; for (int i = 0; i < result.size(); i++) { sum +=
		 * result.get(i).getValue(); } return sum;
		 */
	}

	

	/**
	 * 
	 * @param amount
	 *            value is we want to withdraw
	 * @param valuable
	 *            List is collect Valuable
	 * @return List
	 */
	/*
	 * public List<Valuable> withdraw1(double amount, List<Valuable> valuable) {
	 * if (amount < 0) return null;
	 * 
	 * List<Valuable> with = new ArrayList<Valuable>();
	 * Collections.sort(valuable, comparator); // Collections.reverse(valuable);
	 * List<Valuable> coins2 = new ArrayList<Valuable>(); ArrayList<Integer>
	 * number = new ArrayList<Integer>(); coins2.addAll(valuable); double num =
	 * amount; // do{
	 * 
	 * for (int i = 0; i < valuable.size(); i++) { if
	 * (valuable.get(i).getValue() <= num) { with.add(valuable.get(i));
	 * 
	 * number.add(i); num -= valuable.get(i).getValue();
	 * 
	 * } }
	 * 
	 * if (num > 0) { /* List<Valuable> with1 = withdraw2( amount, valuable);
	 * if(with1.size()!=0){ List<Valuable> fall= new ArrayList<Valuable>();
	 * return fall; } else { List<Valuable> a = with1;
	 * 
	 * return a; }
	 */

	/*
	 * } else { for (int i = number.size() - 1; i >= 0; i--) {
	 * valuable.remove(with.get(i)); } } // Did we get the full amount? if (num
	 * > 0) { // failed. Since you haven't actually remove // any coins from
	 * Purse yet, there is nothing // to put back. List<Valuable> fall = new
	 * ArrayList<Valuable>(); return fall; } else { List<Valuable> a = with;
	 * 
	 * return a; }
	 * 
	 * }
	 */

	/**
	 * 
	 * @param amount
	 *            value is we want to withdraw
	 * @param valuable
	 *            List is collect Valuable
	 * @return List
	 */
	/*
	 * public List<Valuable> withdraw2(double amount, List<Valuable> valuable) {
	 * if (amount < 0) return null;
	 * 
	 * List<Valuable> with = new ArrayList<Valuable>();
	 * Collections.sort(valuable, comparator); Collections.reverse(valuable);
	 * List<Valuable> coins2 = new ArrayList<Valuable>(); ArrayList<Integer>
	 * number = new ArrayList<Integer>(); coins2.addAll(valuable); double num =
	 * amount; // do{
	 * 
	 * for (int i = 0; i < valuable.size(); i++) { if
	 * (valuable.get(i).getValue() <= num) { with.add(valuable.get(i));
	 * 
	 * number.add(i); num -= valuable.get(i).getValue();
	 * 
	 * } }
	 * 
	 * if (num > 0) {
	 * 
	 * } else { for (int i = number.size() - 1; i >= 0; i--) {
	 * valuable.remove(with.get(i)); } } // Did we get the full amount? if (num
	 * > 0) { // failed. Since you haven't actually remove // any coins from
	 * Purse yet, there is nothing // to put back. List<Valuable> fall = new
	 * ArrayList<Valuable>(); return fall; } else { List<Valuable> a = with;
	 * 
	 * return a; }
	 * 
	 * }
	 */

}
