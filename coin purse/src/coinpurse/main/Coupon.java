package coinpurse.main;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Coupon extends AbstractValuable{
	private String color;
	private double value;
	private Map<String,Double> price = new HashMap<String,Double>(); 
	/**
	 * 
	 * @param color banknote
	 */
	public Coupon(String color){
		this.color= color;
		this.price.put("Red", 100.0);
		this.price.put("Blue", 50.0);
		this.price.put("Green", 20.0);
		if(this.price.containsKey("Red")){
			this.value=price.get("Red");
		}
		else if(this.price.containsKey("Blue")){
			this.value=price.get("Blue");
		}
		else{
			this.value=price.get("Green");
		}
	}
	/**
	 * !...
	 * Valuable.getValue
	 * @return double
	 */
	public double getValue() {
		return this.value;
	}
	
	@Override
	public String toString() {
		String a = this.color+" coupon";
		return a;
	}
	
	
	
	
}
