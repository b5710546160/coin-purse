package coinpurse.main;
/**
 * 
 * @author kitipoom
 *
 */
abstract class AbstractValuable implements Valuable{

	public  int compareTo(Valuable a){
		Valuable other =  a;
	    int num =0;
	    if(other == null) num=-1;
	    if(this.getValue()<other.getValue())num= -1;
	    if(this.getValue()==other.getValue())num= 0;
	    else num=1;
	    return num;
	    
    
	}
	public boolean equals(Object obj) {
    	if(obj==null){
    		return false;
    	}
    	else if(!obj.getClass().equals(this.getClass())){
    		return false;
    	} else
			return this.getValue() != ((Valuable)obj).getValue() ? false : true;
    }
	/**
	 * @return
	 * !get value.
	 */
	  
}
