package coinpurse.main;

import java.awt.Container;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;

/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class PurseStatus extends JFrame implements Observer {
	private static final int WIDTH = 500;
	private static final int HEIGHT = 75;
	private JLabel num1Label, num2Label, resultLabel, resultLabel1,
			  resultLabel2, sp1, sp2, sp3, sp4, sp5, sp6;
	private JTextField num1TF, num2TF, resultTextField,
			  re50, re20, re10, re5, re1;
	private JButton go;
	private  JProgressBar progess;
	/**
	 * 
	 */
	public PurseStatus() {

		setTitle("Purse Status");
		setSize(WIDTH, HEIGHT);
		num1TF = new JTextField(10);
		num2TF = new JTextField(10);
		resultTextField = new JTextField(10);
		
		num1Label = new JLabel("");
		num2Label = new JLabel("");
		resultLabel = new JLabel("EMPTY");
		resultLabel1 = new JLabel("");
		resultLabel2 = new JLabel("");
		sp1 = new JLabel("");
		sp2 = new JLabel("");
		sp3 = new JLabel("");
		sp4 = new JLabel("");
		sp5 = new JLabel("");
		sp6 = new JLabel("");

		progess = new JProgressBar();
		
		go = new JButton("Go!");
		Container pane = getContentPane();
		GridLayout aGrid = new GridLayout(2, 1);
		pane.setLayout(aGrid);
		pane.add(resultLabel);
		pane.add(progess);

	}

	/**
	 * 
	 */

	public void run() {
		this.pack();
		this.setVisible(true);
	}

	/**
	 * 
	 * @param args
	 *            ...
	 */
	public static void main(String[] args) {

	}

	@Override
	public void update(Observable subject, Object info) {
		if (subject instanceof Purse) {
			Purse purse = (Purse) subject;
			if (purse.isFull()) {
				progess.setValue(100);
				resultLabel.setText("Status is Full ");
			} else {
				int capacity = (int) ((int) purse.getCapacity()-purse.count());
				int capacity1 = purse.count()*100/purse.getCapacity();
				progess.setValue(capacity1);
				resultLabel.setText("EMPTY  " + capacity);
			}
		} else {
			
			resultLabel.setText("No");
		}
	}
}
