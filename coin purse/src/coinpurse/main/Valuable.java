package coinpurse.main;
/**
 * !Use get Value.
 * @author Kitipoom Kongpetch
 * 
 *
 */
public interface Valuable extends Comparable<Valuable>{
	/**
	 * 
	 * @return double.
	 */
	public double getValue( );
}
