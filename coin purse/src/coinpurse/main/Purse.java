package coinpurse.main;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.WithdrawStrategy;



 


/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Kitipoom Kongpetch
 */
public class Purse extends Observable{
    /** Collection of coins in the purse. */
	private final ValueComparator comparator = new ValueComparator();
	private Valuable valable;
	private WithdrawStrategy strategy;
    private List<Valuable> money;
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {

    	this.capacity=capacity;
    	money = new ArrayList<Valuable>();
    	super.setChanged();
		super.notifyObservers(this);
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { return money.size(); }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	if (money.size()==0){return 0.0;}
    	double result=0;
    	for(int i =0;i<money.size();i++){
    		result+=money.get(i).getValue();
    	}
    	return result;
    }
   
    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
   
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	return money.size()==this.getCapacity() ? true : false;
        
        
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Valuable coin ) {
        // if the purse is already full then can't insert anything.
        
    	if(!isFull()){
    		if(coin.getValue()>0) {
    			this.money.add(coin);
    			super.setChanged();
    			super.notifyObservers(this);
    			return true;
    		}
    		else{
    			
    			return false;
    		}
    	}
    	
    	else {
    		
    		return false;
    	}
    }
   
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
    	 
        //if ( ??? ) return ???;
        /*if(amount<0)return null;
        
        List<Valuable> with = new ArrayList<Valuable>();
        Collections.sort(money, comparator);
        Collections.reverse(money);
        List<Valuable> coins2= new ArrayList<Valuable>();
        ArrayList<Integer> number =new ArrayList<Integer>();
        coins2.addAll(money);
        double num = amount;
        //do{
         
        for(int i =0;i<money.size();i++){
        	if(money.get(i).getValue()<=num){
        		with.add(money.get(i));
        		//coins2.remove(i);
        		number.add(i);
        		num-=money.get(i).getValue();
        		
        	}	
        }
      
        if(num>0){
        		
        }
        else{
        	for(int i =number.size()-1;i>=0;i--){
            	money.remove(with.get(i));
        	}
        }
       // }while(num!=0||first==0);
        
	   /*
		* One solution is to start from the most valuable coin
		* in the purse and take any coin that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the coins from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a coin that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the coins from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* coins from list that
		* are equal (using Coin.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove coins one-by-one.		
		*/
		


		// Did we get the full amount?
		/*if ( num > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			return null;
		}
		else{
			Valuable[] a = new Valuable[with.size()]; 
			a = with.toArray(a);
			return a;
    	 
        }*/
    	if(amount<=0)return null;
    	List<Valuable> with = strategy.withdraw( amount, money);
    	if(with!=null){
    		Valuable[] a = new Valuable[with.size()]; 
 			a = with.toArray(a);
 			super.setChanged();
			super.notifyObservers(this);
 			return a; 
    	}
    	else{
    		return null;
    	}
	}
    /**
     * 
     * @param strategy interface class
     */
    public void setWithdrawStrategy(WithdrawStrategy strategy){
    	this.strategy =strategy;
    	super.setChanged();
		super.notifyObservers(this);
    }
    /** 
     * @return a
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
       
    	String a = this.money.size()+" coins with value "+this.getBalance();
    	return a;
    }

}
