package coinpurse.main;

import java.util.Observable;
import java.util.Observer;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class PurseObserver implements Observer{
	/**
	 * 
	 */
	public PurseObserver(){
		
	}
	@Override
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse) subject;
			int balance = (int) purse.getBalance();
			System.out.println("Balance is : " + balance);
		}
		if (info != null)System.out.println(info); 
	}
	

}
