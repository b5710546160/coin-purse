package coinpurse.main;

import java.util.Observer;

import javax.swing.JFrame;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
 

/**
 * @author Kitipoom Kongpetch
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param arg notused
     */
	public static void main(String[] arg){
	
		WithdrawStrategy e =new GreedyWithdraw() ;
		WithdrawStrategy recursive =new RecursiveWithdraw(  ) ;
		// System.out.println("Purse contains ");
        // 1. create a Purse
    	Purse a = new Purse(6);
    	a.setWithdrawStrategy(recursive);
    	
    	JFrame aCalculatorGui = new PurseBalance();
		aCalculatorGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		a.addObserver((Observer) aCalculatorGui);
		
		JFrame aCalculatorGui1 = (JFrame) new PurseStatus();
		aCalculatorGui1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		a.addObserver((Observer) aCalculatorGui1);

		aCalculatorGui.setVisible(true);
		aCalculatorGui1.setVisible(true);
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog b = new ConsoleDialog(a);
        // 3. run() the ConsoleDialog
    	b.run();

    }
}
