package coinpurse.main;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Kitipoom Kongpetch
 */
public class Coin extends AbstractValuable{

    /** !Value of the coin.*/
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
    }

/**
 * @return
 * !get value.
 */
    public double getValue(){
    	return this.value;
    }
/**
 * @return tag
 * @param obj
 * !boolean.
 */
    

    @Override
public String toString() {
    	String a = this.getValue()+"-Baht coin";
    	return a;
    }
}

