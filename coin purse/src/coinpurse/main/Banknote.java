package coinpurse.main;
/**
 * 
 * @author Kitipoom Kongpetch
 *
 */
public class Banknote extends AbstractValuable{
	private double value;
	private long serialnum;
	private static long nextserialnum =1000000;
	/**
	 * 
	 * @param value double
	 */
	public Banknote(double value){
		this.value=value;
		this.serialnum= nextserialnum;
		getNextSerialNumber();
	}
	/**
	 * 
	 */
	public static void getNextSerialNumber() {
		nextserialnum++;
	}
	/**
	 * @return boolean
	 */
	public double getValue() {

		return this.value;
	}
	@Override
	public String toString() {
		String a = this.getValue()+"-Baht Banknote"+ this.serialnum;
		return a;
	}


}
