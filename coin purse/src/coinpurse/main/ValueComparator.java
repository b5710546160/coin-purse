package coinpurse.main;
import java.util.Comparator;
/**.
 * !Sort value.
 * @author Kitipoom Kongpetch
 *
 */
public class ValueComparator implements Comparator<Valuable>{
	/**
	 * !compare.
	 * @param a retyyuyu
	 * @param b weertt
	 * @return value 
	 */
	public int compare(Valuable a, Valuable b) {
		// compare them by value.  This is easy.
		if(a.getValue()>b.getValue()){
			return -1;
		}
		if(a.getValue()<b.getValue()){
			return 1;
		}
		else{
			return 0;
		}
		
	
	}
}
